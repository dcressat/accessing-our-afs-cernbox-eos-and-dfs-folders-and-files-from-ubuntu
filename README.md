# How from Ubuntu you can easily access, manage, copy and move your folders and files on your AFS, CERNBox/EOS and DFS to Accessing AFS/CERNBox/EOS/DFS folder and files

### 1. Install the necessary KDE Plasma desktop as we we rely on the KDE Dolphin file manager to access and manage file on your  AFS CERNBox and DFS

`sudo apt-get -y update && sudo apt-get -y upgrade $$ sudo apt-get install install tasksel && sudo tasksel install kubuntu-desktop && sudo reboot`

### 2. From a terminal or by searching for it in Ubuntu application start the Dolphin file manager as in the example below:

![Dolphin1.png](/img/Dolphin1.png)

### 3. Go into the Dolphin file manager by clicking on the 3 lines menu as below and enable “Configure Dolphin”. I Highly recommend that you also enable “Show Menu bar” at the same time.

![Dolphin2.png](/img/Dolphin2.png)

### 4. Then configure the Dolphin file manager according the screenshots below:

![Dolphin1_Config3.png](/img/Dolphin1_Config3.png)

![Dolphin1_Config4.png](/img/Dolphin1_Config4.png)

### 5. Once you have configure the Dolphin file manager as instructed above it will should then display as below:

![Dolphin1_Config5.png](/img/Dolphin1_Config5.png)

### Now that you have installed the Dolphin file manager you can easily use it to access and manage, copy and move files between your local computer and your LXPlus AFS, CERNBox and DFS workspace, and even directly  files to edit text your files in your on your AFS, CERNBox and DFS workspace by right-clicking on and choosing to edit them with KATE.

### 6. Accessing your LXPlus AFS workspace, folders and files

In the location bar use the following syntax to access your LXPlus folder and files :
`fish://YOUR_CERN_USER_NAME@lxplus.cern.ch/afs/cern.ch/user/YOUR_CERN_USER_NAME_INITIAL/YOUR_CERN_USER_NAME`

It will ask you 1st to trust the CERN SSL certificate and for your name password as in the example below:

![Dolphin1_Acccessing_The_AFS2.png](/img/Dolphin1_Acccessing_The_AFS2.png)

Then when asked what kind of KDE wallet system to use, for convenience purpose, choose the “Classic Blowfish encryted file” as in the example below:

![Dolphin1_Acccessing_The_AFS3.png](/img/Dolphin1_Acccessing_The_AFS3.png)

Then it is safe to choose to not enter any password and press on the “OK” button as in the example below:

![Dolphin1_Acccessing_The_AFS4.png](/img/Dolphin1_Acccessing_The_AFS4.png)

And confirm it as in the example below:

![Dolphin1_Acccessing_The_AFS5.png](/img/Dolphin1_Acccessing_The_AFS5.png)

And then you will have access to your AFS workspace which will allow you copy, move folders and  files back and force between your computer and your AFS workspace and even directly edit text files in your AFS workspace, if you right-click on them and choose to edit them with the “Kate” editor.

![Dolphin1_Acccessing_The_AFS8.png](/img/Dolphin1_Acccessing_The_AFS8.png)

### 7. Accessing your CERNBox workspace, folders and files

In the location bar use the following syntax to access your CERNBox folder and files: `webdavs://cernbox.cern.ch/cernbox/webdav/eos/user/YOUR_CERN_USER_NAME_INITIAL/YOUR_CERN_USER_NAME`
You will be then asked to provide your CERN User Name and password like in the example below:

![Dolphin12_CERNBox1.png](/img/Dolphin12_CERNBox1.png)

And once you have done it, will be access your CERNBox workspace like below and just like for your AFS workspace you will be able to copy, move folders and files back and force between your computer and your CERNBox workspace and even directly edit text files in your CERNBox workspace, if you right-click on them and choose to edit them with the “Kate” editor, just like you can do it with your AFS workspace.

![Dolphin12_CERNBox3.png](/img/Dolphin12_CERNBox3.png)

### 8. Accessing your DFS workspace, folders and files

In the location bar use the following syntax to access your DFS folders and files folder and files like in the example below:
`webdavs://dfs.cern.ch/dfs/`

![Dolphin12_DFS1.png](/img/Dolphin12_DFS1.png)

You will then be asked to provide your CERN user name and password like in the example below:

![Dolphin12_DFS2.png](/img/Dolphin12_DFS2.png)

And then as before and in the example you will copy files and folders over which you have access rights.

![Dolphin12_DFS3.png](/img/Dolphin12_DFS3.png)

And of course also access to your own DFS folders to manage copy,  move your folders and files as before and like in the example below below:

![Dolphin12_DFS4.png](/img/Dolphin12_DFS4.png)
